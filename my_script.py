"""Dummy Python scripts."""


def hello(name: str) -> str:
    """Print hello with a name."""
    return f"Hello {name}"


def double(n: int) -> int:
    """Two times the initial value."""
    return 2 * n
