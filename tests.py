"""Test dummy scripts."""

from my_script import hello, double


def test_hello():
    """Test hello function."""
    assert hello("world!") == "Hello world!"


def test_double():
    """Test double function."""
    assert double(3) == 6
