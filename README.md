Formation Git - Partie 2 : les forges logicielles
=================================================
_2024-05-13 - Benoît Seignovert - Osuna, CNRS UAR-3281, Nantes Université_

[![Presentation cover](cover.png)](https://slides.com/seignovert/formation-git/)

- Support online: [slides.com/seignovert/formation-git](https://slides.com/seignovert/formation-git/)
- Export PDF: [`formation-git-part_2.pdf`](https://gitlab.univ-nantes.fr/osuna/formation/git/-/raw/main/formation-git-part_2.pdf)

---

- Wooclap: [app.wooclap.com/FORMATIONGIT](https://app.wooclap.com/FORMATIONGIT/)
- Feedback: [`wooclap_feedback.pdf`](wooclap_feedback.pdf)


Liens utiles
------------
- **Partie 1 : Présentation de Git par Guy Moebs**: [gitlab.univ-nantes.fr/moebs-g/Git_NuTS](https://gitlab.univ-nantes.fr/moebs-g/Git_NuTS) (internal).

---
Licenses
--------
- Repo: [MIT](LICENSE)
- Présentation: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)
